/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.zui.jencidus;

import cz.cvut.zui.jencidus.csp.CSP;

/**
 *
 * @author dusanjencik
 */
public class CSPMain {
    
    public static void main(String[] args) {
        CSP csp = new CSP();
        csp.load(args[0]);
        
        System.out.print("Processing...");
        long start = System.currentTimeMillis();
        if (csp.solve()) {
            System.out.println("\rSolved in: " + (System.currentTimeMillis() - start) + " ms");
            System.out.println("Evaluation cycles: " + csp.count);
            System.out.println("Number of solutions: " + csp.solutions.size());
            for (int i = 0; i < csp.solutions.size(); i++) {
                System.out.print("\nSolution #" + i + "\n" + csp.solutions.get(i));
            }
            // Stačí odkomentovat a výsledek se zobrazí v GUI
            if (args.length == 2 && args[1].equals("-g")) {
                new Nonogram_GUI(csp);
            }
            csp.writeResults();
        } else {
            System.out.println("\rThere is no solution.");
        }
    }
}
