/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.zui.jencidus.csp;

import java.util.ArrayList;

/**
 * Proměnná v CSP
 *
 * @author dusanjencik
 */
public class Variable extends Cell implements Comparable<Variable> {

    public ArrayList<char[]> list;

    public Variable(int index, boolean isRow) {
        super(index, isRow);
        this.list = new ArrayList<>();
    }

    @Override
    public int compareTo(Variable o) {
        return list.size() - o.list.size();
    }

}
