/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.zui.jencidus.csp;

import java.util.LinkedList;

/**
 * Třída pro hranovou konzistenci
 *
 * @author dusanjencik
 */
public class Arc {

    public Variable var1, var2;

    public Arc(Variable v1, Variable v2) {
        this.var1 = v1;
        this.var2 = v2;
    }

    /**
     * Kontrola hranové nekonzistence
     *
     * @return true, pokud sloupec nemá nic společného se řádkem (=> je smazán)
     */
    public boolean isInconsistent() {
        boolean remove = false;
        int i = var1.index, j = var2.index, k = 0;
        LinkedList<Integer> deleteList = new LinkedList<>();
        for (char[] a : var1.list) {
            boolean same = false;
            for (char[] b : var2.list) {
                if (a[j] == b[i]) {
                    same = true;
                    break;
                }
            }
            if (!same) {
                deleteList.addFirst(k);
                remove |= true;
            }
            k++;
        }
        while (!deleteList.isEmpty()) {
            int id = deleteList.pop();
            var1.list.remove(id);
        }
        return remove;
    }
}
