/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.zui.jencidus.csp;

/**
 * Omezující pravidlo
 *
 * @author dusanjencik
 */
public class Rule {

    public int length;
    public char color;

    public Rule(int size, char color) {
        this.length = size;
        this.color = color;
    }
}
