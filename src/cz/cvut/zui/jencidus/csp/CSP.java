/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.zui.jencidus.csp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * Hlavní třída implementující CSP řešící nonogram
 *
 * @author dusanjencik
 */
public class CSP {

    public Rule[][] rulesInRow, rulesInCol;
    public int numRows = 0, numCols = 0;
    private PriorityQueue<Variable> sortedVariables;
    private Item[] solutionInRow, solutionInCol;
    private Variable[] rowsComb;
    private Variable[] colsComb;
    private int solX, solY;
    public ArrayList<String> solutions;
    public long count = 0;
    private String filename;
    private static final char empty = '_';

    public CSP() {
        solutions = new ArrayList<>();
    }

    /**
     * Načítá křížovku
     *
     * @param inputFile cesta k souboru
     */
    public void load(String inputFile) {
        filename = inputFile;
        try (BufferedReader br = new BufferedReader(new FileReader(new File(inputFile)))) {
            String line;
            int i = 0;
            while ((line = br.readLine()) != null) {
                String[] st = line.split(",");
                if (i == 0) {
                    numRows = Integer.parseInt(st[0]);
                    numCols = Integer.parseInt(st[1]);
                    solutionInRow = new Item[numRows];
                    solutionInCol = new Item[numCols];
                    rowsComb = new Variable[numRows];
                    colsComb = new Variable[numCols];
                    rulesInRow = new Rule[numRows][];
                    rulesInCol = new Rule[numCols][];
                    sortedVariables = new PriorityQueue<>(numRows + numCols);
                } else {
                    if (i <= numRows) {
                        rulesInRow[i - 1] = new Rule[st.length / 2];
                    } else {
                        rulesInCol[i - numRows - 1] = new Rule[st.length / 2];
                    }
                    for (int j = 0; j < st.length; j += 2) {
                        if (i <= numRows) {
                            rulesInRow[i - 1][j / 2] = new Rule(Integer.parseInt(st[j + 1]), st[j].charAt(0));
                        } else {
                            rulesInCol[i - numRows - 1][j / 2] = new Rule(Integer.parseInt(st[j + 1]), st[j].charAt(0));
                        }
                    }
                }
                i++;
            }
        } catch (IOException ex) {
            System.out.println("IO exception: " + ex.toString());
            System.exit(1);
        }
    }

    /**
     * Uloží výsledek do souboru <název>.out
     */
    public void writeResults() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filename + ".out")))) {
            if (solutions.isEmpty()) {
                bw.write("null");
            } else {
                String allSolutions = "";
                for (String s : solutions) {
                    allSolutions += s + "\n";
                }
                bw.write(allSolutions.substring(0, allSolutions.length() - 1));
            }
            bw.close();
        } catch (IOException ex) {
            System.out.println("IO exception: " + ex.toString());
            System.exit(1);
        }
    }

    /**
     * Funkce, která řeší křížovku
     *
     * @return true, pokud je řešitelná a má alespoň jedno řešení
     */
    public boolean solve() {
        // Vytvoří proměnné (řádky a sloupce) jako všechny kombinace omezujících podmínek (domain)
        for (int i = 0; i < numRows; i++) {
            rowsComb[i] = generateCombinations(new Variable(i, true), new char[numCols], empty, 0, rulesInRow[i], 0);
        }
        for (int i = 0; i < numCols; i++) {
            colsComb[i] = generateCombinations(new Variable(i, false), new char[numRows], empty, 0, rulesInCol[i], 0);
        }

        // Hranová konzistence
        ac3();

        for (int i = 0; i < numRows; i++) {
            sortedVariables.add(rowsComb[i]);
        }
        for (int i = 0; i < numCols; i++) {
            sortedVariables.add(colsComb[i]);
        }

        backtrack();
        return !solutions.isEmpty();
    }

    /**
     * Generování všech kombinací proměnných v CSP pro řádek / sloupec
     *
     * @param line řádek / sloupec
     * @param seq již nagenerovaná kombinace
     * @param prevColor předchozí barva
     * @param i index posunu
     * @param rules seznam pravidel
     * @param ruleIndex index na seznam pravidel
     * @return
     */
    private Variable generateCombinations(Variable line, char[] seq, char prevColor, int i, Rule[] rules, int ruleIndex) {
        if (i < seq.length) {
            if (ruleIndex < rules.length) {
                Rule rule = rules[ruleIndex];
                if (((prevColor != empty && prevColor != rule.color) || prevColor == empty) && i + rule.length - 1 < seq.length) {
                    for (int j = 0; j < rule.length; j++) {
                        seq[i + j] = rule.color;
                    }
                    generateCombinations(line, seq, rule.color, i + rule.length, rules, ruleIndex + 1);
                    for (int j = 0; j < rule.length; j++) {
                        seq[i + j] = empty;
                    }
                }
            }
            seq[i] = empty;
            generateCombinations(line, seq, empty, i + 1, rules, ruleIndex);
        } else if (ruleIndex >= rules.length) {
            line.list.add(seq.clone());
        }
        return line;
    }

    /**
     * Vyzkouší všechny možné kombinace a zjišťuje, zda-li se neobjeví řešení
     */
    private void backtrack() {
        count++;
        if (sortedVariables.size() == 0) {
            String[] rows = new String[numRows];
            for (Item item : solutionInRow) {
                rows[item.index] = "";
                for (int j = 0; j < numCols; j++) {
                    rows[item.index] += item.value[j];
                }
            }
            String res = "";
            for (String row : rows) {
                res += row + "\n";
            }
            solutions.add(res);
            return;
        }
        Variable variable = sortedVariables.poll();
        for (char[] line : variable.list) {
            if (isConsistent(line, variable)) {
                if (variable.isRow) {
                    solutionInRow[solX++] = new Item(line, variable.index, variable.isRow);
                    backtrack();
                    solutionInRow[--solX] = null;
                } else {
                    solutionInCol[solY++] = new Item(line, variable.index, variable.isRow);
                    backtrack();
                    solutionInCol[--solY] = null;
                }
            }
        }
        sortedVariables.add(variable);
    }

    /**
     * Kontroluje konzistenci řádku / sloupce
     *
     * @param value řádek / sloupec
     * @param cell odkaz na proměnnou v CSP
     * @return
     */
    private boolean isConsistent(char[] value, Cell cell) {
        if (cell.isRow) {
            for (int i = 0; i < solY; i++) {
                if (value[solutionInCol[i].index] != solutionInCol[i].value[cell.index]) {
                    return false;
                }
            }
        } else {
            for (int i = 0; i < solX; i++) {
                if (value[solutionInRow[i].index] != solutionInRow[i].value[cell.index]) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Provede pomocí hranové konzistence ořezání všech nagenerovaných
     * proměnných v CSP
     */
    private void ac3() {
        LinkedList<Arc> q = new LinkedList<>();
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                q.add(new Arc(rowsComb[i], colsComb[j]));
                q.add(new Arc(colsComb[j], rowsComb[i]));
            }
        }

        while (!q.isEmpty()) {
            Arc arc = q.poll();
            if (arc.isInconsistent()) {
                if (arc.var1.isRow) {
                    for (int j = 0; j < numCols; j++) {
                        q.add(new Arc(arc.var1, colsComb[j]));
                    }
                } else {
                    for (int i = 0; i < numRows; i++) {
                        q.add(new Arc(arc.var1, rowsComb[i]));
                    }
                }
            }
        }
    }
}
