/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.zui.jencidus.csp;

/**
 * Sloupec / řádek
 *
 * @author dusanjencik
 */
public class Item extends Cell {

    public char[] value;

    public Item(char[] value, int index, boolean isRow) {
        super(index, isRow);
        this.value = value;
    }
}
