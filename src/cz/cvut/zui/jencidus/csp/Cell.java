/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.zui.jencidus.csp;

/**
 * Společná část části proměnné v CSP
 *
 * @author dusanjencik
 */
public abstract class Cell {

    public int index;
    public boolean isRow;

    public Cell(int index, boolean isRow) {
        this.index = index;
        this.isRow = isRow;
    }

}
