/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.zui.jencidus;

import cz.cvut.zui.jencidus.csp.CSP;
import cz.cvut.zui.jencidus.csp.Rule;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * Graficky zobrazí nonogram a všechna jeho řešení, mezi kterými lze procházet mezerníkem
 * @author dusanjencik
 */
public class Nonogram_GUI extends JFrame {

    private CSP csp;
    private int rows = 30, colmns = 20, maxX, maxY;
    private int actual = 0;

    public Nonogram_GUI(final CSP csp) {
        this.csp = csp;
        JPanel panel = new JPanel();
        getContentPane().add(panel);
        maxX = max(csp.rulesInRow);
        maxY = max(csp.rulesInCol);
        setSize((csp.numRows + maxX + 1) * rows + 50, (csp.numCols + maxY + 1) * colmns + 50);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - getHeight()) / 2);
        setLocation(x, y);
        getRootPane().registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        getRootPane().registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (actual < csp.solutions.size() - 1) {
                    actual++;
                } else {
                    actual = 0;
                }
                repaint();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        setVisible(true);
    }

    private int max(Rule[][] lst) {
        int max = 0;
        for (Rule[] lst1 : lst) {
            if (lst1.length > max) {
                max = lst1.length;
            }
        }
        return max;
    }

    private Color decodeColor(char color) {
        switch (color) {
            case '#':
                return Color.GRAY;
            case 'G':
                return Color.GREEN;
            case 'R':
                return Color.RED;
            case 'O':
                return Color.ORANGE;
            case 'Y':
                return Color.YELLOW;
            case 'B':
                return Color.BLACK;
            case '_':
                return Color.WHITE;
        }
        return Color.GRAY;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        float dx = (getWidth() - 50) / (float) (csp.numCols + maxX), dy = (getHeight() - 50) / (float) (csp.numRows + maxY);
        g2.drawString((actual + 1) + "/" + csp.solutions.size() + " [space to change]", 10, 35);

        String[] line = csp.solutions.get(actual).split("\n");
        for (int i = 0; i < csp.numCols; i++) {
            for (int j = 0; j < csp.numRows; j++) {
                Rectangle2D r2d = new Rectangle2D.Float(25 + (maxX) * dx + i * dx, 25 + (maxY) * dy + j * dy, dx, dy);
                g.setColor(decodeColor(line[j].charAt(i)));
                g2.fill(r2d);
                g.setColor(Color.BLACK);
            }
        }
        for (int i = 0; i < csp.numCols; i++) {
            int mx = csp.rulesInCol[i].length;
            for (int k = 0; k < mx; k++) {
                Rectangle2D r2d = new Rectangle2D.Float(25 + (maxX) * dx + i * dx, 25 + (maxY - 1) * dy - k * dy, dx, dy);
                g.setColor(decodeColor(csp.rulesInCol[i][mx - 1 - k].color));
                g2.fill(r2d);
                if (csp.rulesInCol[i][mx - 1 - k].color == 'B') {
                    g.setColor(Color.WHITE);
                } else {
                    g.setColor(Color.BLACK);
                }
                g2.drawString("" + csp.rulesInCol[i][mx - 1 - k].length,
                        (maxX) * dx + 35 + i * dx, (maxY - 1) * dy + 45 - k * dy);
            }
            g.setColor(Color.BLACK);
            g2.draw(new Line2D.Float(25f + (i + maxX + 1) * dx, 25f + maxY * dy, 25 + (i + maxX + 1) * dx, getHeight() - 25f));
        }
        for (int i = 0; i < csp.numRows; i++) {
            int mx = csp.rulesInRow[i].length;
            for (int k = 0; k < mx; k++) {
                Rectangle2D r2d = new Rectangle2D.Float(25 + (maxX - 1) * dx - k * dx, 25 + (maxY) * dy + i * dy, dx, dy);
                g.setColor(decodeColor(csp.rulesInRow[i][mx - 1 - k].color));
                g2.fill(r2d);
                if (csp.rulesInRow[i][mx - 1 - k].color == 'B') {
                    g.setColor(Color.WHITE);
                } else {
                    g.setColor(Color.BLACK);
                }
                g2.drawString("" + csp.rulesInRow[i][mx - 1 - k].length,
                        (maxX - 1) * dx + 35 - k * dx, (maxY) * dy + 45 + i * dy);
            }
            g.setColor(Color.BLACK);
            g2.draw(new Line2D.Float(25f + maxX * dx, 25 + (i + maxY + 1) * dy, getWidth() - 25f, 25 + (i + maxY + 1) * dy));
        }
        g.setColor(Color.black);
        g2.setStroke(new BasicStroke(3));
        g2.draw(new Line2D.Float(25f + (maxX) * dx, 25 + (maxY) * dy, getWidth() - 25f, 25 + (maxY) * dy));
        g2.draw(new Line2D.Float(25 + (maxX) * dx, 25f + (maxY) * dy, 25 + (maxX) * dx, getHeight() - 25f));
        g2.draw(new Line2D.Float(getWidth() - 25f, 25 + (maxY) * dy, getWidth() - 25f, getHeight() - 25f));
        g2.draw(new Line2D.Float(25 + (maxX) * dx, getHeight() - 25f, getWidth() - 25f, getHeight() - 25f));
    }
}
