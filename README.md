# Nonogram CSP #

School homework

## Assingment ##
Your task is to write an algorithm which solves a drawn crossword puzzle. The drawn crossword puzzles are brain teasers, in which there is a legend with number placed around the grid used to draw a picture. Each number in the legend sets the number of the following boxes filled with colour which belongs to the given number. The following rules hold:

- between two blocks of the boxes filled with the same colour there is always at least one empty box
- between the boxes filled with different colour there does not have to be an empty box
- the order of the numbers in the legend sets the order of the blocks of the boxes